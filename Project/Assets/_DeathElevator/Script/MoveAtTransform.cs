﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class MoveAtTransform : MonoBehaviour {

    public Transform goAt;
    public float distanceToBeThere=0.3f;
    public float reduceMoveWhenThere=3f;
    public float force = 5f;
    public float wantedSpeed = 6;

   
   
	public void Update() {

        if (!goAt) return;

        AddForce();
        if (Vector2.Distance(transform.position, goAt.position) < distanceToBeThere)
        {

            //Debug.Log("WTF");
            GetComponent<Rigidbody2D>().velocity /= reduceMoveWhenThere;
            goAt = null;
        }

        
	
	}

    private void AddForce()
    {
        Vector2 velocity = GetComponent<Rigidbody2D>().velocity;
        if (velocity.x < -wantedSpeed || velocity.x > wantedSpeed) {
           
            return; }
 
        Vector2 direction = (goAt.position - transform.position).normalized;

        GetComponent<Rigidbody2D>().AddForce(direction * (force * Time.deltaTime), ForceMode2D.Force);
        
       
    }
}

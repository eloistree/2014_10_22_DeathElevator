﻿using UnityEngine;
using System.Collections;

public class CameraTwoPointTriggerFocus : CameraFocusElement
{

    public float priotityFocus = 10f;
    public float size = 10f;

    public Transform fromTarget;
    public Transform whereToFocus;
    public float pourcent=0.3f;

    public void Start()
    {
        if (!whereToFocus) whereToFocus = transform;
    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.gameObject.tag.Equals("Player")) return;
        CameraFocus.AddFocus(this);
    }

    public void OnTriggerExit2D(Collider2D col)
    {
        if (!col.gameObject.tag.Equals("Player")) return;
        CameraFocus.RemoveFocus(this);
    }


    public override Vector3 GetCameraPosition()
    {
        return fromTarget.position + (whereToFocus.position - fromTarget.position) * pourcent;
    }

    public override float GetCameraSize()
    {
        return size;
    }

    public override float GetPriority()
    {
        return priotityFocus;
    }
}

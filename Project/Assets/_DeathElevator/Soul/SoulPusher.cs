﻿using UnityEngine;
using System.Collections;

public class SoulPusher : MonoBehaviour {


    public Transform whereToGo;
    public string soulName = "Soul";

    public void OnTriggerEnter2D(Collider2D col) {
        if (whereToGo == null) return;
        if (!col.gameObject.name.Contains(soulName)) return;
        MoveAtTransform move = col.GetComponent<MoveAtTransform>() as MoveAtTransform;
        move.goAt = whereToGo;

    }
}

﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Animator))]
public class LemmingAnimator : MonoBehaviour {

    private Animator animator;
    
    public string isWalkingBoolName = "IsWalking";
    public string isFallingBoolName = "IsFalling";
    public string isImbalanceBoolName = "IsImbalance";

    public bool IsWalking
    {
        get { return animator.GetBool(isWalkingBoolName); }
        set { animator.SetBool(isWalkingBoolName, value); }
    }
    public bool IsFalling
    {
        get { return animator.GetBool(isFallingBoolName); }
        set { animator.SetBool(isFallingBoolName, value); }
    }
    public bool IsImbalance
    {
        get { return animator.GetBool(isImbalanceBoolName); }
        set { animator.SetBool(isImbalanceBoolName, value); }
    }
    
    
    
    void Awake() 
    {
        animator = GetComponent<Animator>() as Animator;
    }
	
}

﻿using UnityEngine;
using System.Collections;

public class LemmingSoulState : MonoBehaviour {

    public LemmingAnimator lemmingAnimator;
    public Checker groundChecker;
	public Checker[] balanceChecker;
    public Rigidbody2D rigBody2D;
    public float walkingVelocitySensibility=0.1f;
    public enum Direction {Left, Right }
    public bool inverseDirection;

    public AudioSource fallingSound;
    void Awake()
    {
        if (lemmingAnimator == null) { Debug.LogWarning("No animator define !", this.gameObject); }
        if (groundChecker == null) { Debug.LogWarning("No ground checker  define !", this.gameObject); }
        if (rigBody2D == null) { Debug.LogWarning("No rigidbody2D define !", this.gameObject); }
    
    }

	void Update () {

        bool isWalking = IsWalking();
        bool isFalling = IsFalling();
		bool isBalancing = IsBalancing();

        if (isWalking != lemmingAnimator.IsWalking)
            lemmingAnimator.IsWalking = isWalking;
        if (isFalling != lemmingAnimator.IsFalling) { 
            lemmingAnimator.IsFalling = isFalling;
            if (fallingSound != null)
                fallingSound.Play();
            
        }
		if (isBalancing != lemmingAnimator.IsImbalance)
			lemmingAnimator.IsImbalance = isBalancing;


        SetGoodDirection();

    }

    private void SetGoodDirection()
    {
        if (rigBody2D == null) return;
        Direction currentDir;
        if (rigBody2D.velocity.x > 0.1f)
            currentDir = Direction.Right;
        else if (rigBody2D.velocity.x < -0.1f)
            currentDir = Direction.Left;
        else return;
        Vector3 scale = transform.localScale;
        scale.x = currentDir == Direction.Right ? Mathf.Abs(scale.x) : -Mathf.Abs(scale.x);
        scale.x = inverseDirection ? -scale.x : scale.x;
        transform.localScale = scale;
    }

    private bool IsWalking()
    {
        if (rigBody2D == null) return false;
        return Mathf.Abs(rigBody2D.velocity.x) > walkingVelocitySensibility;
    }
    private bool IsFalling()
    {
        if (groundChecker== null) return false;
        return ! groundChecker.IsColliding2D();
    }

	private bool IsBalancing ()
	{
		for(int i=0;i<balanceChecker.Length;i++){
			if(balanceChecker[i].IsColliding2D())

				return true;
		}
		return false;
	}
}

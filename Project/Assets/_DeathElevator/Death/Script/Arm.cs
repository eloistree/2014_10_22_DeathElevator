﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class Arm : MonoBehaviour {

    public string activeBoolName = "IsActive";
    private Animator animator;
    public bool debug=true;
    public KeyCode keyCodeTest=KeyCode.LeftArrow;

    public void Awake()
    {
        animator = GetComponent<Animator>() as Animator;
    }
    public void SetArmState(bool pushOn) 
    {
        animator.SetBool(activeBoolName, pushOn);
    }

    public void Update() { 
     
        if(debug)
            if (Input.GetKeyDown(keyCodeTest))
            {
                SetArmState(true);
            }
            else if (Input.GetKeyUp(keyCodeTest))
            {
                SetArmState(false);
            }
    }

}

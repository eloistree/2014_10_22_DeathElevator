﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {
	
	public Texture2D menutex;
	public Texture2D retrytex;
	public Texture2D highlighttex;
	private int pastlvl;
	
	private Rect menurect=new Rect(0.362F*Screen.width,0.636F*Screen.height,0.157F*Screen.width,0.271F*Screen.height);
	private Rect retryrect=new Rect(0.571F*Screen.width,0.636F*Screen.height,0.157F*Screen.width,0.271F*Screen.height);
	
	bool init=false;
	
	private int highlight=1;
	
	private float lastchange=0;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Joystick1Button0)){
			switch(highlight){
			case 0:
				Application.LoadLevel("Menu");
				break;
			case 1:
				Application.LoadLevel(PlayerPrefs.GetInt("level",1));
				break;
			}
		}
		if(Time.timeSinceLevelLoad - lastchange > 0.25f){
			if(Input.GetAxis("JLH")+Input.GetAxis("JRH")>0.95){
				highlight=(highlight+1)%2;
				lastchange=Time.timeSinceLevelLoad;
			}else if(Input.GetAxis("JLH")+Input.GetAxis("JRH")<-0.95){
				highlight=(highlight+2-1)%2;
				lastchange=Time.timeSinceLevelLoad;
			}
		}	
		
	}
	
	void OnLevelWasLoaded(int pastlvl){
		this.pastlvl=pastlvl;
	}
	
	void OnGUI(){
		menurect=new Rect(0.362F*Screen.width,0.636F*Screen.height,0.157F*Screen.width,0.271F*Screen.height);
		retryrect=new Rect(0.571F*Screen.width,0.636F*Screen.height,0.157F*Screen.width,0.271F*Screen.height);
		if(!init){
			init=true;
			GUI.skin.button=GUIStyle.none;
		}
		if(GUI.Button(menurect,menutex)){
			Application.LoadLevel("Menu");
		}
		if(GUI.Button(retryrect,retrytex)){
			Application.LoadLevel(pastlvl);
		}
		
		switch(highlight){
		case 0:
			GUI.Button(menurect,highlighttex);
			break;
		case 1:
			GUI.Button(retryrect,highlighttex);
			break;
		}
		
		
	}
}



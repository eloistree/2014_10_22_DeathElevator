﻿using UnityEngine;
using System.Collections;

public class MovingElevator : MonoBehaviour {

	private bool invertaxes=false;
	public float puissance;
	public float maxangle;
	public GameObject reactL;
	public GameObject reactR;

	public float minparticule;
	public float maxparticule;

	public Arm armL;
	public Arm armR;

	public QuickRotation[] dentR;
	public QuickRotation[] dentL;

	public float minsound;

	public bool isactivated=false;
    public bool startAutomatics = true;


	void Start () {
		if(PlayerPrefs.GetInt("invert",1)==1)
			invertaxes=true;
		else
			invertaxes=false;
        if (startAutomatics) activedeath();
	}

	void activedeath(){
		if(isactivated)
			return;

		reactL.GetComponent<ParticleSystem>().Play();
		reactR.GetComponent<ParticleSystem>().Play();



		isactivated=true;

	}
	


	void Update () {
		if(!isactivated)
			return;

		float jhl=0F;
		float jhr=0F;
		float left=0F;
		float right=0F;

		if(Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown(KeyCode.JoystickButton3)){
		   	invertaxes = ! invertaxes;
			if(invertaxes)
				PlayerPrefs.SetInt("invert",1);
			else
				PlayerPrefs.SetInt("invert",0);
		}

		if(invertaxes){
			jhl = Input.GetAxis ("JLH");
			jhr = Input.GetAxis ("JRH");
			left = Input.GetAxis ("LT");
			right = Input.GetAxis ("RT");
		}else{
			jhl = Input.GetAxis ("JRH");
			jhr = Input.GetAxis ("JLH");
			left = Input.GetAxis ("RT");
			right = Input.GetAxis ("LT");

		}

		if(left*right>0.9f){
			activedeath();
		}


		reactL.transform.rotation=transform.rotation;
		reactR.transform.rotation=transform.rotation;

		reactL.transform.Rotate(new Vector3(90,0,0));
		reactR.transform.Rotate(new Vector3(90,0,0));

		reactL.transform.Rotate(new Vector3(0,45*jhl,0));
		reactR.transform.Rotate(new Vector3(0,45*jhr,0));


		//Debug.Log("("+left+";"+right+")");

		Vector2 forceL = reactL.transform.TransformDirection (Time.deltaTime * puissance * left * Vector3.back);
		Vector2 forceR = reactR.transform.TransformDirection (Time.deltaTime * puissance * right * Vector3.back);

		Vector2 positionL = reactL.transform.position;
		Vector2 positionR = reactR.transform.position;

			
		GetComponent<Rigidbody2D>().AddForceAtPosition (forceL, positionL);
		GetComponent<Rigidbody2D>().AddForceAtPosition (forceR, positionR);

		armL.SetArmState(left<0.5f);
		armR.SetArmState(right<0.5f);
		
		reactL.GetComponent<ParticleSystem>().startSpeed = minparticule + left * (maxparticule-minparticule);
		reactR.GetComponent<ParticleSystem>().startSpeed = minparticule + right * (maxparticule-minparticule);

		for(int i=0;i<dentR.Length;i++){
			dentR[i].multiplicateur=1+right*4;
		}

		for(int i=0;i<dentL.Length;i++){
			dentL[i].multiplicateur=1+left*4;
		}

		GetComponent<AudioSource>().volume = minsound + (left+right) * 0.5F * (1F-minsound);

	}
}

﻿using UnityEngine;
using System.Collections;

public class Perturbing : MonoBehaviour {

	public float constant;
	public float power;
	private int count=0;
	private Rigidbody2D death;
	private float timer;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(count>0){
			death.AddTorque((Time.timeSinceLevelLoad-timer)*power*Time.deltaTime*Mathf.Sin(Time.timeSinceLevelLoad));
		}
	}

	void OnTriggerEnter2D(Collider2D mort){
		if(mort.gameObject.name=="Elevator"){
			death=mort.GetComponent<Rigidbody2D>();
			count++;
			if(count==1){
				timer=Time.timeSinceLevelLoad;
			}
		}
	}

	void OnTriggerExit2D(Collider2D mort){
		if(mort.gameObject.name=="Elevator"){
			count--;
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class KillableSoul : MonoBehaviour {

	public GameObject effusion1;
	public GameObject effusion2;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void kill(){
		Instantiate(effusion1,transform.position,Quaternion.Euler(new Vector3(300,90,270)));
		Instantiate(effusion2,transform.position,Quaternion.Euler(new Vector3(300,-90,270)));
        SoulLevelManager.DeathSoul();
		Destroy(gameObject);
	}
}

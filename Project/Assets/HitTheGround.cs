﻿using UnityEngine;
using System.Collections;

public class HitTheGround : MonoBehaviour {


	public float SpeedToCrash;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D ground){
		if(ground.relativeVelocity.magnitude > SpeedToCrash){
			GetComponent<KillableSoul>().kill();
		}
	}

}

﻿using UnityEngine;
using System.Collections;

public class SaveSoulZone : MonoBehaviour {

    public string soulName = "Soul";

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.gameObject.name.Contains(soulName)) return;
        LemmingSoulState soul = col.GetComponent<LemmingSoulState>() as LemmingSoulState;
        if (soul) SoulLevelManager.SaveSoul();

    }
}

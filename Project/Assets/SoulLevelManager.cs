﻿using UnityEngine;
using System.Collections;

public class SoulLevelManager : MonoBehaviour {

    public static SoulLevelManager InstanceInScene;

    public int numberInScene = 0;
    public int saveSoulToWin = 3;
    public int deathSoulToLost = 2;

    private int _saveCount;

    public int SaveCount
    {
        get { return _saveCount; }
        set {
            _saveCount = value;
            if (_saveCount >= saveSoulToWin  )
                CurrentLevelManager.NotifyLevelWin();
        }
    }

    private int _deathCount;

    public int DeathCount
    {
        get { return _deathCount; }
        set {
            _deathCount = value;
            if (_deathCount >= deathSoulToLost )
                CurrentLevelManager.NotifyLevelLost();
                
        }
    }

 
    
    

    void Awake() 
    {
        InstanceInScene = this;
        LemmingSoulState[] souls = FindObjectsOfType<LemmingSoulState>() as LemmingSoulState[];
        if (souls == null)
            numberInScene = 0;
        else
            numberInScene = souls.Length;
    }

    public static void SaveSoul()
    {
        if (InstanceInScene) InstanceInScene.SaveCount++;
    }

    public static void DeathSoul()
    {
        if (InstanceInScene) InstanceInScene.DeathCount++;
    }

}
